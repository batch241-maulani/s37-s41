
const Course = require("../models/Course")



module.exports.addCourse = (reqBody,isAdmin) => {

	console.log(isAdmin.isAdmin)



	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price


	})

	return newCourse.save().then((course,error)=>{
		if(!isAdmin.isAdmin){
			if(error){
				return false
			}else{
				return true
			}
		}else{
			return false
		}

	})
	

}


module.exports.getAllCourses = () =>{
	return Course.find({}).then(result => {
		return result
	})
}

module.exports.getAllActiveCourses = () =>{
	return Course.find({isActive: true}).then(result => {
		return result
	})


}


module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result=>{
		return result
	})
}



module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId,updateCourse).then((course, error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})


}

module.exports.archiveCourse = (reqParams,reqBody) => {
	let archiveCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId,archiveCourse).then((course, error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})


}








