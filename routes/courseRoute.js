
const express = require("express")

const router = express.Router()

const auth = require("../auth")


const courseController = require("../controllers/courseController")

router.post("/", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)



	courseController.addCourse(req.body,{isAdmin: userData.isAdmin}).then(resultFromController=> res.send(resultFromController))

})

router.get("/all", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

router.get("/allactive", (req,res) => {
	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController))
})


router.get("/:courseId", (req,res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


router.put("/:courseId",auth.verify, (req, res) =>{

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))


})


router.patch("/:courseId/archive",auth.verify, (req, res) =>{

	courseController.archiveCourse(req.params,req.body).then(resultFromController => res.send(resultFromController))


})








module.exports = router




